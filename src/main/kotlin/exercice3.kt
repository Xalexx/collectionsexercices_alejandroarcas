import java.util.Scanner

val mapOfStudents = mutableMapOf<String ,Int>()

fun main() {
    val sc = Scanner(System.`in`)

    while (true){
        val userInput = sc.next()
        if (userInput.uppercase() == "END") {
            printVotingRankingSorted()
            break
        }
        if (userInput in mapOfStudents.keys) mapOfStudents[userInput] = mapOfStudents[userInput]!! + 1
        else mapOfStudents[userInput] = 1
    }
}

private fun printVotingRankingSorted(){
    for (student in mapOfStudents){
        println("${student.key}: ${student.value}")
    }
}