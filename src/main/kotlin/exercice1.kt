import java.util.*

fun main() {
    val sc = Scanner(System.`in`)
    val collectionOfSigns = mutableMapOf<Int, String>()
    val listOfAnswer = mutableListOf<String>()

    val sizeSigns = sc.nextInt()
    for (times in 1..sizeSigns){
        val meters = sc.nextInt()
        val signText = sc.next()
        collectionOfSigns[meters] = signText
    }

    val sizeQuery = sc.nextInt()
    for (times in 1..sizeQuery){
        val meters = sc.nextInt()
        if (meters in collectionOfSigns.keys) {
            listOfAnswer.add("${collectionOfSigns[meters]}")
        } else listOfAnswer.add("No hi ha cartell")
    }

    for (i in listOfAnswer){
        println(i)
    }
}