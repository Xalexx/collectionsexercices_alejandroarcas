import java.util.*

data class Employee(val dni: String, val name: String, val surname: String, val address: String)

fun main() {
    val sc = Scanner(System.`in`)
    val collectionOfEmployee = mutableMapOf<String, Employee>()

    val numberOfEmployee = sc.nextInt()
    for (i in 1..numberOfEmployee){
        val dni = sc.next()
        val name = sc.next()
        val surname = sc.next()
        sc.nextLine()
        val address = sc.nextLine()

        collectionOfEmployee[dni] = Employee(dni, name, surname, address)
    }
    while (true) {
        val userQuery = sc.next().uppercase()
        if (userQuery == "END") break
        println("\n${collectionOfEmployee[userQuery]!!.name} ${collectionOfEmployee[userQuery]!!.surname} - " +
                "${collectionOfEmployee[userQuery]!!.dni}, ${collectionOfEmployee[userQuery]!!.address}")

    }
}
