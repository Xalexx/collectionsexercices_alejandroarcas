import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)

    val setOfUserInputs = mutableSetOf<String>()

    while (true){
        val userInput = sc.next()
        if (userInput.uppercase() == "END") break
        else {
            if (userInput !in setOfUserInputs){
                setOfUserInputs.add(userInput)
            } else print("\nMEEEC!")
        }
    }
}