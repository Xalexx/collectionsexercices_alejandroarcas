import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)
    val bingoCard = mutableListOf<Int>()

    for (i in 1..10){
        bingoCard.add(sc.nextInt())
    }

    while (bingoCard.size != 0){
        val bingoInput = sc.nextInt()
        if (bingoCard.contains(bingoInput)) bingoCard.remove(bingoInput)
        println("Em queden ${bingoCard.size} números")
    }
    println("BINGO")
}